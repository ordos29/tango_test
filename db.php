<?php

class DB
{
    protected static $_db;

    public static function connection()
    {
        if (isset(static::$_db)) {
            return static::$_db;
        } else {
            try {
                $db = new PDO('mysql:host=localhost;dbname=tango_test;charset=utf8', 'root', '100032');
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                static::$_db = $db;

                return $db;
            } catch (PDOException $e) {
                die("Ошибка подключения к базе: " . $e->getMessage());
            }
        }
    }
}
