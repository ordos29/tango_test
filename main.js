window.onload = function () {
    updateUsersList();
};

$(document).on('click', '[data-uid]', function () {
    var uid = $(this).data('uid');
    var name = $(this).data('name');
    showUserLoginStats(uid, name);
});

function updateUsersList() {
    $.get(
        "/getdata.php",
        {get_users: true},
        function (data) {
            $.each(JSON.parse(data), function (key, value) {
                $("#users ").append('<li data-uid="' + key + '"  data-name="' + value + '"><a href="#"><span class="tab">' + value + '</span></a></li>');
            });
        }
    );
}

function showUserLoginStats(uid, name) {
    $.get(
        "/getdata.php",
        {get_login_dates: true, user_id: uid},
        function (data) {
            $('#name').text(name);
            $('#logins-list').empty();
            $.each(JSON.parse(data), function (key, value) {
                $('#logins-list').append('<tr><td>' + value + '</td></tr>');
            });
        }
    );
    $('#logins-panel').show();
}
