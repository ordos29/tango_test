<html>
<head>
    <title>
        Тестовое задание для Танго Телеком
    </title>
    <script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
            crossorigin="anonymous"></script>
    <script src="/main.js"></script>
</head>
<body>
<div class="col-lg-4 col-lg-offset-4">
    <div class="panel panel-primary ">
        <div class="panel-heading">
            <h3 class="panel-title">Тестовое задание для Танго Телеком</h3>
        </div>
        <div class="panel-body">
            <ul class="nav nav-pills" role="tablist">
                <li role="presentation" class="dropdown">
                    <a href="#" class="dropdown-toggle"
                       data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false"> Список пользователей <span class="caret"></span>
                    </a>
                    <ul id="users" class="dropdown-menu" aria-labelledby="drop6">
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div class="panel panel-info" id="logins-panel" style="display: none">
        <div class="panel-heading">
            <h3 class="panel-title">Даты, когда авторизовался пользователь <span id="name"></span></h3>
        </div>
        <div class="panel-body">
            <table class="table table-hover" id="logins-list">
            </table>
        </div>
    </div>
</body>
</html>
