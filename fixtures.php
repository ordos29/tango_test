<?php

require_once 'db.php';

try {
    DB::connection()->exec("DROP TABLE IF EXISTS `login_stat`");
    DB::connection()->exec("DROP TABLE IF EXISTS `users`");
    DB::connection()->exec("CREATE TABLE `users`(id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255) NOT NULL DEFAULT '')");
    DB::connection()->exec("CREATE TABLE `login_stat`(user_id INT, time TIMESTAMP NOT NULL DEFAULT now())");
    DB::connection()->exec("ALTER TABLE `login_stat`
               ADD CONSTRAINT `user_id_ref`
               FOREIGN KEY (`user_id`)
               REFERENCES `users`(`id`)
               ON DELETE RESTRICT
               ON UPDATE RESTRICT");
} catch (PDOException $e) {
    die("Ошибка создания таблицы: " . $e->getMessage());
}

try {
    $usersIds = [];
    $insertStatement = DB::connection()->prepare("INSERT INTO `users` (`name`) VALUES (:name)");
    $name = 'Виктор';
    $insertStatement->bindParam(':name', $name);
    $insertStatement->execute();
    $usersIds[] = (int)DB::connection()->lastInsertId();
    $name = 'Иван';
    $insertStatement->bindParam(':name', $name);
    $insertStatement->execute();
    $usersIds[] = (int)DB::connection()->lastInsertId();
    $name = 'Павел';
    $insertStatement->bindParam(':name', $name);
    $insertStatement->execute();
    $usersIds[] = (int)DB::connection()->lastInsertId();
    foreach ($usersIds as $usersId) {
        insertLoginStats($usersId);
    }
} catch (PDOException $e) {
    die("Ошибка вставки данных: " . $e->getMessage());
}

function insertLoginStats($userId)
{
    $insertStatement = DB::connection()->prepare("INSERT INTO `login_stat` (`user_id`, `time`) VALUES (:uid , FROM_UNIXTIME(:time))");
    for ($n = 0; $n < 5; $n++) {
        $delta = rand(0, 60 * 60 * 24 * 14);
        $time = time() - $delta;
        $insertStatement->bindParam(':uid', $userId);
        $insertStatement->bindParam(':time', $time);
        $insertStatement->execute();
    }
}
