<?php

require_once 'db.php';

define('CAHCHE_TIME', 60);

function getUsersList()
{
    $memcache = new Memcache;
    $isMemcacheConnected = $memcache->connect('127.0.0.1', 11211);
    if ($isMemcacheConnected) {
        $usersList = $memcache->get('users_list');
        if (!empty($usersList)) {

            return $usersList;
        }
    }
    $usersList = json_encode(getUsersFromBase());
    if ($isMemcacheConnected) {
        $memcache->set('users_list', $usersList, false, CAHCHE_TIME);
        $memcache->close();
    }

    return $usersList;
}

function getLoginStats()
{
    $memcache = new Memcache;
    $isMemcacheConnected = $memcache->connect('127.0.0.1', 11211);
    $userId = (int)$_GET['user_id'];
    if ($isMemcacheConnected) {
        $userLoginList = $memcache->get('log_' . $userId);
        if (!empty($userLoginList)) {

            return $userLoginList;
        }
    }
    $userLoginList = json_encode(getLoginStatsFromBase());
    if ($isMemcacheConnected) {
        $memcache->set('log_' . $userId, $userLoginList, false, CAHCHE_TIME);
        $memcache->close();
    }

    return $userLoginList;
}

function getUsersFromBase()
{
    $statement = DB::connection()->prepare("SELECT * FROM `users`");
    $statement->execute();
    $loginsData = $statement->fetchAll();
    $users = [];
    foreach ($loginsData as $user) {
        $users[$user['id']] = $user['name'];
    }

    return $users;
}

function getLoginStatsFromBase()
{
    $statement = DB::connection()->prepare("SELECT `time` FROM `login_stat` WHERE `user_id` = :uid ORDER BY `time` ASC");
    $userId = (int)$_GET['user_id'];
    $statement->bindParam(':uid', $userId);
    $statement->execute();
    $loginsData = $statement->fetchAll();

    return array_map(function ($value) {
        return $value['time'];
    }, $loginsData);
}
