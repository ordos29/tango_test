<?php

require_once 'lib.php';

if (isset($_GET['get_users'])) {
    echo getUsersList();
} elseif (isset($_GET['get_login_dates']) && !empty($_GET['user_id'])) {
    echo getLoginStats();
} else {
    header("Location: /");
}
